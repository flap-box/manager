import { createSocket } from "dgram"
import { Buffer } from "buffer"
import * as os from "os"
import * as url from "url"
import { promises as fs } from "fs"

import * as cheerio from "cheerio"

import { logger, sleep, parseResponse } from "../tools"
import { fetch } from "../tools"

// A recuring string to put in UPnP requests
const WANIP = "urn:schemas-upnp-org:service:WANIPConnection:1"
const WANPPP = "urn:schemas-upnp-org:service:WANPPPConnection:1"

type PortMapping = {
	externalPort: number
	internalPort: number
	internalClient: string
}

// The PCP (Port Control Protocol) class allow to open, close, and list port mappings using the UPnP protocol
// Ref: https://tools.ietf.org/html/rfc6970
export class PCP {
	private hostname = ""
	private port = 0
	private path = ""

	private inited = false

	private serviceType?: string
	private controlPath?: string

	public readonly ip = Object.values(os.networkInterfaces())
		.flat()
		.filter(i => !i.internal && i.family === "IPv4")
		.map(i => i.address)[0]

	/**
	 * Used to lazily get the IGD information
	 * @return a void promise
	 */
	async init() {
		if (this.inited) {
			return
		}

		logger.debug(`Initialiasing PCP object`)

		this.inited = true

		try {
			// By default, try to use the cached location
			logger.debug(`Trying with cached IGD`)
			await this._init(
				JSON.parse(
					await fs.readFile("/var/lib/manager/igd.txt", "utf8"),
				),
			)
		} catch (error) {
			logger.debug(`Cached IGD failed, make mSearch`)
			// If the cache fail, make a mSearch
			await this._init(await mSearch())
		}
	}

	private async _init(location: url.UrlWithStringQuery) {
		logger.silly(`Initing with ${JSON.stringify(location)}`)

		if (location.hostname === undefined) {
			throw new Error("hostname not found")
		}

		if (location.port === undefined) {
			throw new Error("port not found")
		}

		if (location.path === undefined) {
			throw new Error("path not found")
		}

		this.hostname = location.hostname
		this.port = Number.parseInt(location.port)
		this.path = location.path

		await this.getWanIpConnectionPath()

		logger.debug(
			`PCP object initialized with: ${this.hostname}:${this.port}${
				this.path
			}`,
		)

		await fs.writeFile("/var/lib/manager/igd.txt", JSON.stringify(location))
	}

	/**
	 * Get the external IP
	 * @return a promise containing the external IP as a string
	 */
	public async getExternalIP() {
		await this.init()

		logger.info(`Getting external IP`)

		const externalIp = await cheerio
			.load(await this.soapFetch("GetExternalIPAddress", ""))(
				"NewExternalIPAddress",
			)
			.text()

		logger.debug(`External IP found: ${externalIp}`)

		return externalIp
	}

	/**
	 * Get the first 1000 port mappings
	 * The 1000 limit is arbitrary
	 * @return a promise of an array containing port mappings
	 */
	public async getPortMappings(): Promise<PortMapping[]>
	public async getPortMappings(port: number): Promise<PortMapping | undefined>
	public async getPortMappings(
		port?: number,
	): Promise<PortMapping[] | PortMapping | undefined> {
		await this.init()

		logger.info(`Listing port mappings`)

		const portMappings: PortMapping[] = []

		let i = 0
		try {
			while (i < 1000) {
				portMappings.push(await this._getPortMapping(i++))
			}
		} catch (error) {
			logger.debug(error)
		}

		if (port !== undefined) {
			return portMappings.find(mapping => mapping.externalPort === port)
		} else {
			return portMappings
		}
	}

	// Helper for getPortMapping
	private async _getPortMapping(index: number) {
		logger.silly(`Querying for port mapping at index: ${index}`)

		const xml = cheerio.load(
			await this.soapFetch(
				"GetGenericPortMappingEntry",
				`<NewPortMappingIndex>${index}</NewPortMappingIndex>`,
			),
		)

		const portMapping = {
			externalPort: Number.parseInt(xml("NewExternalPort").text()),
			internalPort: Number.parseInt(xml("NewInternalPort").text()),
			internalClient: xml("NewInternalClient").text(),
		}

		logger.debug(
			`Found a port mapping: ${portMapping.externalPort} => ${
				portMapping.internalClient
			}:${portMapping.internalPort}`,
		)

		return portMapping
	}

	/**
	 * Delete a port mapping to the current host
	 * @param  port the port to close
	 * @return      a void promise
	 */
	public async closePort(port: number) {
		await this.init()

		logger.info(`Closing port ${port} to ${this.ip}`)

		// Mapping for this specific port
		const portMapping = await this.getPortMappings(port)

		// Prevent closing a closed port
		if (portMapping === undefined) {
			return
		}

		// Prevent closing a port that is not forwarded to us
		if (portMapping.internalClient !== this.ip) {
			throw new Error(
				`Can not close port ${port} as it is not forwared to this host.`,
			)
		}

		return this.soapFetch(
			"DeletePortMapping",
			`
				<NewRemoteHost></NewRemoteHost>
				<NewExternalPort>${port}</NewExternalPort>
				<NewProtocol>TCP</NewProtocol>
			`,
		)
	}

	/**
	 * Create a port mapping
	 * @param  port the port to open
	 * @return      a void promise
	 */
	public async openPort(port: number) {
		await this.init()

		logger.info(`Openning port ${port} to ${this.ip}`)

		const portMapping = await this.getPortMappings(port)

		// Prevent opening the port if it is already open but forwarded to an other host
		if (
			portMapping !== undefined &&
			portMapping.internalClient !== this.ip
		) {
			throw new Error(`Port ${port} is already mapped to another host.`)
		}

		// Prevent opening an opened port
		if (portMapping !== undefined) {
			return
		}

		return this.soapFetch(
			"AddPortMapping",
			`
				<NewRemoteHost></NewRemoteHost>
				<NewExternalPort>${port}</NewExternalPort>
				<NewProtocol>TCP</NewProtocol>
				<NewInternalPort>${port}</NewInternalPort>
				<NewInternalClient>${this.ip}</NewInternalClient>
				<NewEnabled>1</NewEnabled>
				<NewPortMappingDescription>Open port ${port} for the FLAP box</NewPortMappingDescription>
				<NewLeaseDuration>0</NewLeaseDuration>
			`,
		)
	}

	/**
	 * IGD query are made to a given path.
	 * This function retreive this path.
	 * @return the path to query for IGD queries
	 */
	private async getWanIpConnectionPath() {
		if (this.controlPath) {
			return this.controlPath
		}

		logger.debug(`Getting controlPath`)

		let response = await fetch({
			hostname: this.hostname,
			port: this.port,
			path: this.path,
			method: "GET",
		})

		// Find path to query for the public IP address
		// TODO - This is an naive approche, it might not be the same data structure in all cases.
		// Maybe change the logic be looking up SSPD response's data structure.
		const wanIpConnectionPath = cheerio
			// "root device deviceList device deviceList device serviceList controlURL",
			.load(response)("serviceType")
			.filter((_i, el) => cheerio(el).text() === WANIP)
			.parent()
			.find("controlURL")
			.text()

		const wanPppConnectionPath = cheerio
			// "root device deviceList device deviceList device serviceList controlURL",
			.load(response)("serviceType")
			.filter((_i, el) => cheerio(el).text() === WANPPP)
			.parent()
			.find("controlURL")
			.text()

		if (wanIpConnectionPath !== "") {
			this.serviceType = WANIP
			this.controlPath = wanIpConnectionPath
		}

		if (wanPppConnectionPath !== "") {
			this.serviceType = WANPPP
			this.controlPath = wanPppConnectionPath
		}

		if (this.serviceType === undefined) {
			throw new Error("wanIpConnection and wanPppConnection not found")
		}

		logger.debug(`controlPath found ${this.controlPath}`)

		return this.controlPath
	}

	private async soapFetch(action: string, body: string) {
		// Create an UPnP query
		const payload = `
			<?xml version="1.0"?>
			<s:Envelope
				xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"
				s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">
				<s:Body>
					<u:${action} xmlns:u="${this.serviceType}">
						${body}
					</u:${action}>
				</s:Body>
			</s:Envelope>`

		return fetch(
			{
				hostname: this.hostname,
				port: this.port,
				path: this.controlPath,
				method: "POST",
				headers: {
					host: this.hostname,
					SOAPACTION: `"${this.serviceType}#${action}"`,
					"content-type": 'text/xml; charset="utf-8"',
					"content-length": payload.length,
				},
			},
			payload,
		)
	}
}

/**
 * Utility to find the IGD
 * Broadcast a M-SEARCH query and return the IGD.
 * The listening will timeout after 3 seconds.
 * @return a promise containing the IGD
 */
export async function mSearch() {
	logger.debug(`Searching for IGD`)

	return new Promise<url.UrlWithStringQuery>(async (resolve, reject) => {
		const socket = createSocket("udp4")

		socket.addListener("message", message => {
			// UPNP is HTTP over UDP, so we need to parse the message as an HTTP response
			let httpResponse
			try {
				httpResponse = parseResponse(message.toString())
			} catch (error) {
				logger.error(error)
				return
			}

			if (
				httpResponse.code === "200" &&
				httpResponse.headers.location !== undefined &&
				httpResponse.headers.usn !== undefined &&
				httpResponse.headers.usn.includes(
					"urn:schemas-upnp-org:device:InternetGatewayDevice:1",
				)
			) {
				socket.close()
			} else {
				return
			}

			// Parse the location
			const parsedLocation = url.parse(httpResponse.headers.location)

			if (!parsedLocation.href) {
				reject(
					new Error(
						`Location's href is undefined: ${parsedLocation}`,
					),
				)
			}

			logger.debug(`IGD found: ${parsedLocation.href}`)

			resolve(parsedLocation)
		})

		socket.addListener("error", error => {
			socket.close()
			reject(error)
		})

		socket.addListener("listening", () => {
			logger.silly(`Broadcasting M-SEARCH query`)

			// WARNING: do not unindent
			// This is an HTTP request and it is white spaces sensitive
			const query = Buffer.from(
				`M-SEARCH * HTTP/1.1
Host:239.255.255.250:1900
ST:urn:schemas-upnp-org:device:InternetGatewayDevice:1
MAN:"ssdp:discover"
MX:1

`,
				"ascii",
			)

			// Broadcast the M-SEARCH query to 239.255.255.250:1900
			socket.send(
				query,
				0,
				query.length,
				1900,
				"239.255.255.250",
				error => {
					if (error) {
						reject(error)
					}
				},
			)
		})

		socket.bind()

		// Wait 3 seconds, then close the socket
		await sleep(3000)
		// Wrapped in a try/catch to avoid exception when the socket is already closed
		try {
			socket.close()
			reject(new Error("M-SEARCH timeout after 3 seconds"))
		} catch {}
	})
}
