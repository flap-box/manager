## Core API of FLAP

---

This is a manager for FLAP. It is used to:

-   open ports on the local gateway
-   generate configurations for other services
-   generate TSL certificate

#### Cloning

It is preferable to clone the main project:

`git clone --recursive git@gitlab.com:flap-box/flap.git`

We need the `--recursive` flag so submodules are also cloned.

#### Todos

-   [ ] Setup recursive tasks
-   [ ] Ask for static IP to the gateway
-   [ ] Handle WANPPPConnection (orange livebox pro)
