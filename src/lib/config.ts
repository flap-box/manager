import { promises as fs } from "fs"
import { logger } from "../tools"

/**
 * Generate configuration from env variables for services
 */
export async function generateConfig(env: { [key: string]: string }) {
	// Nginx
	logger.debug(`Generating configurations for Nginx`)
	await _generateConfig("/flap/nginx/config/nginx.conf", env)
	await _generateConfig("/flap/nginx/config/conf.d/flap.conf", env)
	await _generateConfig("/flap/nginx/config/conf.d/sogo.conf", env)
	await _generateConfig("/flap/nginx/config/conf.d/nextcloud.conf", env)
	// PostgreSQL
	logger.debug(`Generating configurations for PostgreSQL`)
	await _generateConfig("/flap/postgres/scripts/setup.sql", env)
	await _generateConfig("/flap/postgres/postgres.env", env)
	// LDAP
	logger.debug(`Generating configurations for LDAP`)
	await _generateConfig("/flap/ldap/ldap.env", env)
	// FLAP core
	logger.debug(`Generating configurations for FLAP core`)
	await _generateConfig("/flap/core/core.env", env)
	// Nextcloud
	logger.debug(`Generating configurations for nextcloud`)
	await _generateConfig("/flap/nextcloud/nextcloud.env", env)
	// SOGo
	logger.debug(`Generating configurations for SOGo`)
	await _generateConfig("/flap/sogo/config/sogo.conf", env)
	await _generateConfig("/flap/sogo/sogo.env", env)
}

/*
 * Helper to generate a configuration.
 * It loads the file's template apply the env to the template and write the new file.
 * It will derive the template's name from the passed file ("file.txt" ==> "file.template.txt")
 * @param file the file name
 * @param env the env to apply to the template
 */
async function _generateConfig(file: string, env: { [key: string]: string }) {
	// transform "core.env" to "core.template.env"
	const templateName = file.replace(/(\.)((?!\.).)+$/, ".template$&")

	// Load the template
	const template = await fs.readFile(templateName, "utf8")

	const conf = Object.keys(env).reduce(
		// Replace occurances of the key in the template by the key's value
		// We need to add a "\" before $ so it is not interpreted in the RegExp
		(template, key) =>
			template.replace(new RegExp(`\\$${key}`, "g"), env[key]),
		// Pass the template as the reduce init value
		template,
	)

	// Write conf
	await fs.writeFile(file, conf)
}
