#!/usr/bin/env node

import "core-js"

import {
	PCP,
	generateConfig,
	generateCert,
	getPwdFromFile,
	getDomainInfo,
} from "./lib"
import { logger } from "./tools"

import * as program from "commander"

program
	.version("0.1.0")
	.description("CLI interface to manage a FLAP box")
	.option("-d, --debug", "Enable debug logging")
	.option("-v, --verbose", "Enable debug and silly logging")

program
	.command("port")
	.description("Manage ports redirections")
	.option("-o, --open <port>", "Open a port.", parseInt)
	.option("-c, --close <port>", "Close a port.", parseInt)
	.option("-l, --list", "List port mapping.")
	.action(async (prgm: program.Command) => {
		try {
			if (prgm.open) {
				await new PCP().openPort(Number.parseInt(prgm.open))
			} else if (prgm.close) {
				await new PCP().closePort(Number.parseInt(prgm.close))
			} else if (prgm.list) {
				console.log(await new PCP().getPortMappings())
			}
		} catch (error) {
			logger.error(error)
		}
	})

program
	.command("getip")
	.description("Get the ip of the box, internal or external.")
	.option("-i, --internal", "Get internal IP.")
	.option("-e, --external", "Get external IP.")
	.action(async (prgm: program.Command) => {
		try {
			if (prgm.internal) {
				console.log(await new PCP().ip)
			} else if (prgm.external) {
				console.log(await new PCP().getExternalIP())
			}
		} catch (error) {
			logger.error(error)
		}
	})

program
	.command("config")
	.description("Manipulate the box's configuration.")
	.option(
		"-g, --generate",
		"Generate the configuration files for each services.",
	)
	.option("-s, --show", "Show the configuration.")
	.action(async (prgm: program.Command) => {
		const domainInfo = await getDomainInfo()

		// Load env variables from FLAP's data
		const env = {
			DOMAIN_NAME: domainInfo.name,
			ADMIN_PWD: await getPwdFromFile("/var/lib/manager/adminPwd.txt"),
			SOGO_DB_PWD: await getPwdFromFile("/var/lib/manager/sogoDbPwd.txt"),
			NEXTCLOUD_DB_PWD: await getPwdFromFile(
				"/var/lib/manager/nextcloudDbPwd.txt",
			),
		}

		logger.debug(`Env is ${JSON.stringify(env)}`)

		try {
			if (prgm.generate) {
				await generateConfig(env)
			} else if (prgm.show) {
				console.log(env)
			}
		} catch (error) {
			logger.error(error)
		}
	})

program
	.command("tls")
	.description("Generate TLC certificates for Nginx")
	.action(async () => {
		try {
			await generateCert(await getDomainInfo())
		} catch (error) {
			logger.error(error)
		}
	})

program.parse(process.argv)
