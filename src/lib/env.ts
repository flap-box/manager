import * as crypto from "crypto"
import { promises as fs } from "fs"

import { logger } from "../tools"
import { DomainNameInfo, DomainNameProviders } from "./models"

/*
 * Load the domain information from FLAP's core data
 */
export async function getDomainInfo(): Promise<DomainNameInfo> {
	logger.debug(`Loading domain info`)

	try {
		return JSON.parse(
			await fs.readFile("/var/lib/manager/domainInfo.txt", "utf8"),
		)
	} catch {
		// Default to LAN wide domain name
		if (process.env.NODE_ENV === "production") {
			return {
				name: "flap.local",
				provider: DomainNameProviders.Local,
			}
		} else {
			return {
				name: "flap.localhost",
				provider: DomainNameProviders.Local,
			}
		}
	}
}

/*
 * This function loads a password written in a file.
 * If the file does not exists or if the file is empty, it will generate a new one.
 * @param file the file from which to load the password
 * @return the password
 */
export async function getPwdFromFile(file: string) {
	logger.debug(`Getting password from ${file}`)

	try {
		const password = await fs.readFile(file, "utf8")
		if (password.length === 0) {
			throw undefined
		}
		return password
	} catch (error) {
		const value = crypto.randomBytes(32).toString("hex")
		await fs.writeFile(file, value)
		return value
	}
}
