import { execFileSync } from "child_process"
import { DomainNameInfo } from "./models"

export async function generateCert(domainInfo: DomainNameInfo) {
	execFileSync("./scripts/certificates/generateCerts.sh", [
		domainInfo.name,
		domainInfo.provider,
		...(domainInfo.authentication !== undefined
			? [domainInfo.authentication]
			: []),
	])
}
