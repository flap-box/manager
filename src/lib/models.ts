export enum DomainNameProviders {
	Localhost = "localhost",
	Local = "local",
	DuckDns = "duckdns",
}

export interface DomainNameInfo {
	name: string
	provider: DomainNameProviders
	authentication?: string
}
