# BUILD STAGE
FROM node:10.15.3-stretch-slim as builder
WORKDIR /manager
COPY ./ ./
RUN npm install
# Build
RUN npm run build
# Reinstall node_module but with --only=production flag to not install all dependencies
# The generated node_modules will be used in the runtime stage
RUN rm -rf ./node_modules
RUN npm install --only=production

# RUNTIME STAGE
FROM node:10.15.3-stretch-slim
WORKDIR /manager
COPY ./scripts ./scripts
COPY --from=builder /manager/build ./build
COPY --from=builder /manager/package.json ./package.json
COPY --from=builder /manager/node_modules ./node_modules
# Install certbot
RUN apt-get update && \
    apt-get install -y certbot && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
ENTRYPOINT ["node", "/manager/build/main.js"]
