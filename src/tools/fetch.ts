import * as http from "http"

import { logger } from "./logger"

/**
 * Utility to make a HTTP request
 * @param  options options og the HTTP request
 * @param  body    the body of the request
 * @return         a promise containing the body of the response
 */
export async function fetch(options: http.RequestOptions, body?: string) {
	logger.silly(
		`Fetching ${options.method} ${options.hostname}:${options.port || 80}${
			options.path
		}`,
	)

	const request = http.request(options)

	request.end(body)

	return new Promise<string>((resolve, reject) => {
		request.addListener("response", (response: http.ServerResponse) => {
			let body = ""

			response.addListener("data", chunk => (body += chunk))
			response.addListener("end", () => {
				if (response.statusCode !== 200) {
					reject(
						new Error(
							`Bad status code: ${response.statusCode} ${
								response.statusMessage
							} ${body}`,
						),
					)
				} else {
					resolve(body)
				}
			})
		})
	})
}
